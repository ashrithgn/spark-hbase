import Framework.Exception.ConfigurationException;
import Framework.Exception.DaoException;
import Framework.Exception.JsonreaderException;
import Framework.Init;
import Framework.Logger.Log;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import Framework.ORM.OutputProcessor;
import Utlity.Jsonreader;
import Utlity.Token;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by root on 5/12/16.
 */
public class App {

    public static void main(String[] args) throws JsonreaderException, ConfigurationException, IOException, DaoException {
        Framework.Init bootstrap = new Framework.Init(System.getProperty("user.dir") + "/conf/applicationConfig.json");
        AdapterImplementation dao= Init.getDao();
        Map<String,Object> a = new HashMap<String,Object>();
        Map<String,Object> b = new HashMap<String,Object>();
        Map<String,Object> c = new HashMap<String,Object>();
        b.put("testing1","testing1");
        c.put("testing","testing2");
        List<String> roles =  new ArrayList<>();
        roles.add("admin");
        roles.add("super admin");
        roles.add("admin");
        a.put("username","a");
        a.put("password","ghghgh");
        List<Map> mylist = new ArrayList<>();
        mylist.add(b);
        mylist.add(c);
        a.put("mylist",mylist);
        a.put("role",roles);
        System.out.println(a);
        dao.add(a,"1");
        OutputProcessor op = new OutputProcessor(bootstrap.config);
        System.out.println(op.process(dao.getbyId("1",null,1,0)));
    }

}