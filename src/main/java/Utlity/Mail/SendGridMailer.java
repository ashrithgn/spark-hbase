package Utlity.Mail;

import com.sendgrid.*;


/**
 * Created by costrategix on 9/20/16.
 */
public class SendGridMailer {
    private static SendGridMailer ourInstance = null;
    private static String inapikey;

    public static SendGridMailer getInstance(String apikey) {
        inapikey =apikey;
        ourInstance = new SendGridMailer();
        return ourInstance;
    }

    private SendGridMailer(){

    }


    public boolean mail(String to, String from, String subject, String body){
        SendGrid mailer = new SendGrid(inapikey);
        Email fromEmail = new Email(from);
        Email toEmail = new Email(to);
        Content content = new Content();
        content.setType("text/html");
        content.setValue(body);
        Mail mail = new Mail(fromEmail, subject, toEmail, content);
        Request request = new Request();
        request.method = Method.POST;
        request.endpoint = "mail/send";
        try {
            request.body = mail.build();
            Response response = mailer.api(request);
        }catch (Exception e){
        	e.printStackTrace();
            return false;
        }
        return true;
    }


}
