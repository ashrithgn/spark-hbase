package Utlity;

import Framework.Exception.JsonreaderException;
import com.google.gson.Gson;


import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang.StringUtils.trim;

/**
 * Created by root on 5/12/16.
 */
@SuppressWarnings("ALL")
public class Jsonreader {

    public static Map<String,Object> fromFile(String path) throws JsonreaderException {
        String input = "";
        try {
            BufferedReader in = new BufferedReader(new FileReader(path));
            String tmp;
            while ((tmp = in.readLine() )!= null){
                input+= tmp;
            }

        } catch (FileNotFoundException e) {
            throw new JsonreaderException("file not found",1);
        } catch (IOException e) {
            throw new JsonreaderException("unable to read file ",2);
        }if(input == null || (input.isEmpty())){
            throw new JsonreaderException("empty file",3);
        }
        try{
            Map<String,Object> objects =  new Gson().fromJson(input,Map.class);
            return objects;
        }catch (Exception e){
            throw new JsonreaderException("malformed json",4);
        }

    }

    public static Map<String,Object> fromFolder(String path) throws JsonreaderException {
        Map<String,Object> Json = new HashMap<String,Object>();
        File[] files = new File(path).listFiles();
        for (File f:files) {
            if(f.getName().contains(".json")){
                String input = "";
                try {
                    BufferedReader in = new BufferedReader(new FileReader(f));
                    String tmp;
                    while ((tmp = in.readLine() )!= null){
                        input+= tmp;
                    }
                } catch (FileNotFoundException e) {
                    throw new JsonreaderException(f.getName() + " file not found",1);
                } catch (IOException e) {
                    throw new JsonreaderException(f.getName() + " unable to read file ",2);
                }
                if(input != null && (!input.isEmpty())){
                    try{
                        Map<String,Object> objects =  new Gson().fromJson(trim(input),Map.class);
                        System.out.println();
                        Json.put(f.getName().split("\\.")[0],objects);
                    }catch (Exception e){
                        throw new JsonreaderException("malformed json",4);
                    }
                }
            }
        }
        return Json;
    }

}
