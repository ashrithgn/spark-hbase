package Utlity;

import Framework.Exception.Http.AuthException;
import io.jsonwebtoken.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;


/**
 * Created by costrategix on 9/19/16.
 */
public class Token {
    String jwtKey;
    String encryptKey;
    Encryption encrypt;
    
    @SuppressWarnings("unused")
	private Token(){
    	
    }

    public Token(String jwtkey,String encryptKey){
    	this.jwtKey = jwtkey;
        this.encryptKey = encryptKey;
        encrypt = new Encryption(encryptKey);
    }
    
    public String buildToken(String id, String issuer, String subject,String role,long ttlMillis) throws IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
        if(role == null){
            role = "default";
        }

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(jwtKey);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, SignatureAlgorithm.HS256.getJcaName());
        String finalRole = role;
        JwtBuilder builder = Jwts.builder().setId(encrypt.encrypt(id))
                .setIssuedAt(now)
                .setSubject(encrypt.encrypt(subject))
                .setIssuer(encrypt.encrypt(issuer))
                .claim("role",encrypt.encrypt(finalRole))
                .signWith(SignatureAlgorithm.HS256, signingKey);
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }
        return builder.compact();
    }

    public HashMap<String, Object> parseToken(String jwt) throws AuthException {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(jwtKey))
                    .parseClaimsJws(jwt).getBody();
            HashMap<String, Object> result = new HashMap<>();
            result.put("id", encrypt.decrypt(claims.getId()));
            result.put("subject", encrypt.decrypt(claims.getSubject()));
            result.put("issuer", encrypt.decrypt(claims.getIssuer()));
            result.put("expiration", claims.getExpiration());
            result.put("role",encrypt.decrypt(claims.get("role").toString()));
            return result;

        } catch (ExpiredJwtException e) {
            e.printStackTrace();
            throw new AuthException(e.getLocalizedMessage(),"21");
        }
        catch (MalformedJwtException e){
            throw new AuthException(e.getLocalizedMessage(),"22");
        }
        catch ( UnsupportedJwtException e){
            throw new AuthException(e.getLocalizedMessage(),"23");
        }
        catch (Exception e){
            throw new AuthException(e.getLocalizedMessage(),"29");
        }
    }

}
