package Utlity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.security.SecureRandom;

/**
 * Created by Ashrith 9/16/16.
 */

public class Hash {

    public static String hash(String password ,int seedLength) {

        String PassWordHashAlgo = "Bcrypt";
        String passwordHased = "";
        switch(PassWordHashAlgo){
            default :
                SecureRandom sr = new SecureRandom();
                sr.generateSeed(seedLength);
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(6,sr);
                passwordHased= passwordEncoder.encode(password);
                break;
        }
        return passwordHased;
    }

    public static boolean compareHash(String hashedValue,String password,int seedLength){
        String PassWordHashAlgo = "bcrypt";
        boolean retval = false;
        switch(PassWordHashAlgo){
            default :
                SecureRandom sr = new SecureRandom();
                sr.generateSeed(seedLength);
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(6,sr);
                if (passwordEncoder.matches(password, hashedValue)) {
                    retval = true;
                }
                break;
        }

        return retval;
    }
}
