package Utlity.Sms;

import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by costrategix on 9/21/16.
 */
public class TwilloSmsSender  {
    private static TwilloSmsSender ourInstance = null;
    private static String ACCOUNT_SID;
    private static String AUTH_TOKEN;
    private static TwilioRestClient client;
    private static String fromNumber;
    private static MessageFactory messageFactory;

    public static TwilloSmsSender getInstance(String sid ,String token,String number) throws TwilioRestException {
        if(number == null || number.equals(null)){
            fromNumber = "+12566458451";
        }else{
            fromNumber = number;
        }
        if(ourInstance == null){
            ACCOUNT_SID = sid;
            AUTH_TOKEN =token;
            ourInstance = new TwilloSmsSender();
        }
        return ourInstance;
    }

    private TwilloSmsSender()  {
        client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
        messageFactory = client.getAccount().getMessageFactory();

    }

    public boolean sendSms(String to,String body) throws TwilioRestException{
        ArrayList<NameValuePair> params = new ArrayList<>();

        params.add(new BasicNameValuePair("From",fromNumber));
        params.add(new BasicNameValuePair("To",to));
        params.add(new BasicNameValuePair("Body",body));

        Message message =messageFactory.create(params);
        message.getSid();
        return true;
    }
}
