package Framework.Validations;

import Framework.Conf.FrameworkConfiguration;
import Framework.Exception.DaoException;
import Framework.Exception.ValidationException;
import Framework.ORM.Adapter.Hbase;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.util.*;

import static Framework.Init.dao;

/**
 * Created by ashrith on 2/1/17.
 */
public class HbaseDataIntigirtyCheck {
    private  FrameworkConfiguration conf;
    private List<String> fieldIndex;

    public HbaseDataIntigirtyCheck(FrameworkConfiguration configuration) {
        conf = configuration;
        fieldIndex = FrameworkConfiguration.getFieldIndex();
    }



    public void validate(String cf,Object value,String id,boolean update) throws ValidationException {
        //valid field check
        String cfm = cf;
        if(!cf.contains("#")){
            if(!fieldIndex.contains(cf)){
                throw new ValidationException(cf +" is Invalid parameter key",9);
            }
        }else{

            if(!fieldIndex.contains(cf)){
                System.out.println(cf + "----------------------- cf");
                if(!fieldIndex.contains(cf)){
                    if(cf.contains("#object")){
                        cf = cf.substring(0,cf.lastIndexOf("object"));
                        cf = cf.substring(0,cf.lastIndexOf("#"));
                    }
                    if(cf.contains("#list")){
                        cf = cf.substring(0,cf.lastIndexOf("list"));
                        cf = cf.substring(0,cf.lastIndexOf("#"));
                    }
                }

                if(!fieldIndex.contains(cf)){
                    throw new ValidationException(cf +" is Invalid parameter key",9);
                }
            }
        }

        //type check
        typeCheck(value,cf,cfm);
        //null check
        nullCheck(value,cf);
        //unique check
        uniqueCheck(value,cf,id,update);
        //enumrationCheck
        enumCheck(value,cf);
        //
        patternCheck(value,cf);



    }

    private void patternCheck(Object value, String cf) throws ValidationException {
        String[] cfs = cf.split("#");
        Map <String,Object> field = null;
        //get root of object
        Map<String,String> a = new HashMap<>();
        a.put("name",cfs[0]);

        int index = getIndex(conf.getFields(),cfs[0]);//getIndex((List<Map>)field.get("enum"),cfs[i]);


        field = conf.getFields().get(index);

        if(cfs.length > 1){
            int findex;
            for (int i=1; i<=cfs.length-1;i++){
                findex =getIndex((List<Map>)field.get("enum"),cfs[i]);
                if(findex >=0){
                    field = (Map<String,Object>)((List<Map>) field.get("enum")).get(findex);
                }

            }

        }

        if(field.get("pattern") !=null && field.get("pattern").toString().isEmpty()){
            if(!GenericValidations.pattern(value.toString(),field.get("pattern").toString())){
                throw new ValidationException("The "+ value.toString() +"do not match configured regex",10);
            }
        }

    }

    private void enumCheck(Object value, String cf) throws ValidationException {
        String[] cfs = cf.split("#");
        Map <String,Object> field = null;
        //get root of object
        Map<String,String> a = new HashMap<>();
        a.put("name",cfs[0]);

        int index = getIndex(conf.getFields(),cfs[0]);//getIndex((List<Map>)field.get("enum"),cfs[i]);


        field = conf.getFields().get(index);

        if(cfs.length > 1){
            for (int i=1; i<=cfs.length-1;i++){
                Map<String,String> tmp = new HashMap<>();
                tmp.put("name",cfs[i]);
                int findex = getIndex((List<Map>)field.get("enum"),cfs[i]);
                if(findex >=0){
                    if(field.get("enum") instanceof List){
                        if(((List<Map>) field.get("enum")).get(findex) instanceof Map){
                            field = (Map<String,Object>)((List<Map>) field.get("enum")).get(findex);
                        }
                    }
                }


            }
        }

        if(!field.get("type").toString().equals("object")){
            if(value instanceof String ){
                if(field.get("enum") instanceof List){
                    if(!((List<String>)field.get("enum")).contains(value)){
                        throw new ValidationException("invalid enum value",9);
                    }
                }
            }

            if(value instanceof List ){
                if(field.get("enum") instanceof List){
                    for (String val:(List<String>)value) {
                        if(!((List<String>)field.get("enum")).contains(val)){
                            throw new ValidationException("The "+ val +" is not present in the enum list",9);
                        }
                    }
                }
            }
        }
    }



    private void uniqueCheck(Object value, String cf,String id, boolean update) throws ValidationException {
        String[] cfs = cf.split("#");
        Map <String,Object> field = null;
        //get root of object
        Map<String,String> a = new HashMap<>();
        a.put("name",cfs[0]);

        int index = getIndex(conf.getFields(),cfs[0]);//getIndex((List<Map>)field.get("enum"),cfs[i]);


        field = conf.getFields().get(index);

        if(cfs.length > 1){
            if(cfs.length > 1){
                for (int i=1; i<cfs.length-1;i++){
                    Map<String,String> tmp = new HashMap<>();
                    tmp.put("name",cfs[i]);
                    int findex = getIndex((List<Map>)field.get("enum"),cfs[i]);
                    if(field.get("enum") instanceof List){
                        if(((List<Map>) field.get("enum")).get(findex) instanceof Map){
                            field = (Map<String,Object>)((List<Map>) field.get("enum")).get(findex);
                        }
                    }
                }
            }


        }

        if(field.get("unique").toString().equals("true")){
            List<Map> list = null;
            try {
                list = dao.search(cf,value.toString(),1,0);
                System.out.println(list);
            } catch (DaoException e) {
                e.printStackTrace();
            }
            if(list.size() > 0){
                if(!update){
                    throw new ValidationException( value + " value  of  key " + cf + " is not unique",9);
                }
                else{
                    if(!list.get(0).get("key").equals(id)){
                        throw new ValidationException( value + " value  of  key " + cf + " is not unique",9);
                    }

                }
            }
        }
    }



    private void nullCheck(Object value,String cf) throws ValidationException {
        String[] cfs = cf.split("#");
        Map <String,Object> field = null;
        //get root of object
        Map<String,String> a = new HashMap<>();
        a.put("name",cfs[0]);

        int index = getIndex(conf.getFields(),cfs[0]);//getIndex((List<Map>)field.get("enum"),cfs[i]);


        field = conf.getFields().get(index);

        if(cfs.length > 1){
            int findex;
            for (int i=1; i<=cfs.length-1;i++){
                findex =getIndex((List<Map>)field.get("enum"),cfs[i]);
                if(findex >=0){
                    field = (Map<String,Object>)((List<Map>) field.get("enum")).get(findex);
                }

            }

        }

        if(value == null || value.toString().isEmpty()){
            if(field.get("allowNull").toString().equals("false")){
                throw new ValidationException("value cannot be null",8);
            }
        }
    }

    private void typeCheck(Object value, String cf,String cfm) throws ValidationException {
        String[] cfs = cf.split("#");
        Map <String,Object> field = null;
        int index = getIndex(conf.getFields(),cfs[0]);
        field = conf.getFields().get(index);

        if(cfs.length > 1){
            for (int i=1; i<=cfs.length-1;i++){
                System.out.println(index + "--------------- index");
                int findex = getIndex((List<Map>)field.get("enum"),cfs[i]);
                if(findex >= 0){
                    if(field.get("enum") instanceof List){
                        if(field.get("enum") != null && !field.get("enum").toString().isEmpty()){
                            if(field.get("enum") instanceof List ){
                                if(((List<Map>) field.get("enum")).get(findex) != null && ((List<Map>) field.get("enum")).get(findex) instanceof Map){
                                    field = (Map<String,Object>)((List<Map>) field.get("enum")).get(findex);
                                }
                            }

                        }
                    }
                }
            }
        }

        switch (field.get("type").toString()){
            case "string":
                if(value != null || !value.toString().isEmpty()){
                    if(! (value instanceof String)){
                        throw new ValidationException("not a valid string",7);
                    }
                }

                break;
            case "object":
                if(! (cfm.contains("#object")))
                    /*have to find better logic

                     */
                    //throw new ValidationException(cfm +" is not a object ",7);
                    break;

            case "list" :
                if(! (cfm.contains("#list")))
                    throw new ValidationException("is not valid list",7);
                break;
            case "numeric" :
                if(value != null || !value.toString().isEmpty()){
                    if(! (value instanceof Number)){
                        throw new ValidationException("not a valid number",7);
                    }
                }
        }
    }

    private int getIndex(List<Map> fields, String cf) {

        for(int i=0;i<=conf.getFields().size();i++){
            Map<String,Object> field = (Map)fields.get(i);
            if(field.get("name").toString().equals(cf)){
                return i;
            }
        }
        return -1;
    }

}



