package Framework.ORM;

import Framework.Conf.FrameworkConfiguration;
import Framework.Conf.FrameworkHabseConfiguration;
import Framework.Exception.DaoException;
import Framework.Exception.FactoriesException;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import Framework.ORM.Adapter.Hbase;
import Framework.ORM.Facotries.HbaseFactory;
import Framework.ORM.Facotries.MysqlFactory;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * Created by ashrith on 31/12/16.
 */
 public abstract class Dao implements AdapterImplementation {

    private static FrameworkConfiguration c;

    public static AdapterImplementation getInstance(FrameworkConfiguration configuration) throws DaoException {
        c=configuration;
        if(c.getDatabase() == "mysql"){
            try {
                return new MysqlFactory(c);
            } catch (FactoriesException e) {
                throw new DaoException(e.getMessage().substring(0,e.getMessage().indexOf("##")), (Integer.parseInt(e.getMessage().substring(e.getMessage().indexOf("##")))));
            }
        }else{
            try {
                FrameworkHabseConfiguration hfc = new FrameworkHabseConfiguration(c);
            } catch (IOException e) {
                throw new DaoException("unable to run configuration",1);
            }

            return new HbaseFactory(c);
        }
    }

}
