package Framework.ORM;

import Framework.Conf.FrameworkConfiguration;
import com.google.gson.Gson;
import org.apache.avro.generic.GenericData;
import org.apache.commons.collections.map.HashedMap;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.yarn.webapp.hamlet.HamletSpec;
import org.apache.log4j.chainsaw.Main;
import org.w3c.dom.Entity;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by ashrith on 18/1/17.
 */
public class OutputProcessor {
    FrameworkConfiguration conf;
    private OutputProcessor() {
    }

    public OutputProcessor(FrameworkConfiguration c ) {
        conf = c;
    }

    public List<Map<String,Object>> process(Map<String,Object> input){
        List<Map<String,Object>> retval = new ArrayList<>();
        return retval;
    }

    public Map<String,Object> process(List<Map> input){
        Map<String,Object> retval = new HashMap<>();
        Map<String,Integer> string=new HashMap<>(), list=new HashMap<>(),object =new HashMap<>(),listAndObject = new HashMap<>(),objectAndList = new HashMap<>();
        int index = 0;
        for (Map in:input) {
            String qualifier = in.get("qualifier").toString();
            if(qualifier.contains("#list") && qualifier.contains("#object")){
                if(qualifier.indexOf("#list") < qualifier.indexOf("#object")){
                    listAndObject.put(qualifier,index);
                }else{
                    objectAndList.put(qualifier,index);
                }
            }

            else if(qualifier.contains("#list") && !qualifier.contains("#object")){
                list.put(qualifier,index);
            }
            else if (!qualifier.contains("#list") && qualifier.contains("#object")){
                object.put(qualifier,index);
            }
            else if (!qualifier.contains("#list") && !qualifier.contains("#object")){
                if(!qualifier.contains("#")){
                    string.put(qualifier,index);
                    retval.put(qualifier,in.get("value"));
                }else{
                    object.put(qualifier,index);
                }
            }
            index ++;
        }
        //parsing object
        for (String obj:object.keySet()) {
            String keys[] = obj.split("#");
            String value = input.get(object.get(obj)).get("value").toString();
            Map<String,Object> result = retval;
            int objIndex = 0;

            while (objIndex < keys.length){
                String key = keys[index];
                if(objIndex == keys.length-1 ){
                    result.put(key,value);
                }
                else {
                    if(!result.containsKey(key)){
                        result.put(key,new HashMap<String,Object>());
                    }
                    result = (HashMap<String, Object>) result.get(key);
                }
                objIndex ++;
            }

        }
        //parse list
        for (String lis:list.keySet()) {
            ArrayList<String> keys = new ArrayList<String>(Arrays.asList(lis.split("#")));
            int lisIndex = 0;
            String value = input.get(list.get(lis)).get("value").toString();
            while (lisIndex < keys.size() -2) {
                String key = keys.get(lisIndex);
                Map<String,Object> result = retval;
                if(lisIndex == keys.size() -3){
                    if(keys.contains("raw")){
                        result.put(key,value);
                    }
                    else{
                        ArrayList<String> tmp = null;
                        if(result.containsKey(key)){
                            tmp = (ArrayList<String>) result.get(key);
                            tmp.add(value);
                        }else{
                            tmp = new ArrayList<>();
                            tmp.add(value);
                        }
                        result.put(key,tmp);
                    }
                }else{
                    if(!result.containsKey(key)){
                        result.put(key,new HashMap<String,Object>());
                    }
                    result = (HashMap<String,Object>)result.get(key);
                }
                lisIndex ++;
            }

        }
        return retval;
    }
}
