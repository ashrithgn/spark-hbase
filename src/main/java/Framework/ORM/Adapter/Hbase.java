package Framework.ORM.Adapter;

import Framework.Conf.FrameworkConfiguration;
import Framework.Exception.DaoException;
import Framework.Exception.ValidationException;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import Framework.Validations.HbaseDataIntigirtyCheck;
import com.google.gson.Gson;
import org.apache.avro.generic.GenericData;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellScanner;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.*;

/**
 * Created by ashrith on 6/12/16.
 */
public class Hbase implements AdapterImplementation {

    protected static Connection connection;
    protected static FrameworkConfiguration conf;
    protected  static HbaseEntityCollection hbc;


    @Override
    public Boolean add(Map<String, Object> values,String id) throws DaoException {
        hbc = new HbaseEntityCollection();
        add(values,id,null);
        hbc.flush(true);
        return null;
    }

    @Override
    public Boolean update(Map<String, Object> values,String id) throws DaoException {
        hbc = new HbaseEntityCollection();
        add(values,id,null);
        hbc.flush(true);
        return null;
    }

    @Override
    public Boolean Delete(String id, String field) throws DaoException {
        Table t;
        Boolean retval = true;
        ArrayList<Delete> deleteArrayList = new ArrayList<>();

        try {
            t =connection.getTable(TableName.valueOf(conf.getServiceName()));
        } catch (IOException e) {
            throw new DaoException(e.getMessage(),6);
        }
        Get g = new Get(Bytes.toBytes(id));
        g.addFamily(Bytes.toBytes("fields"));

        Result r = null;
        try {
            r = t.get(g);
        } catch (IOException e) {
            retval = false;
            e.printStackTrace();
        }

        if(r != null && !r.isEmpty()){

            for (Cell c:r.listCells()) {
                Boolean deleteStatus =  true;
                System.out.println("status " + deleteStatus);
                System.out.println(Bytes.toString(CellUtil.cloneQualifier(c)));
                if(field != null){
                    if(!field.isEmpty()){
                        if(!Bytes.toString(CellUtil.cloneQualifier(c)).startsWith(field)){
                            deleteStatus = false;
                        }
                    }
                }
                System.out.println("status " + deleteStatus);
                if(deleteStatus){
                    String lValue = Bytes.toString(CellUtil.cloneValue(c));
                    lValue = lValue.replaceAll("#","").toLowerCase();
                    if(!lValue.isEmpty()){
                        List<Map>lkpSearch = search(Bytes.toString(CellUtil.cloneQualifier(c)),lValue,1,0);

                        for (Map l:lkpSearch) {
                            System.out.println(l);
                            if(l.get("key").equals(id)){
                                Delete ld = new Delete(Bytes.toBytes(l.get("value").toString().replaceAll("#","").toLowerCase()));
                                ld.addColumn(Bytes.toBytes("lookup"),Bytes.toBytes(l.get("qualifier").toString()+"#"+l.get("key").toString()));
                                deleteArrayList.add(ld);
                            }
                        }
                    }
                    Delete d = new Delete(CellUtil.cloneRow(c));
                    d.addColumn(CellUtil.cloneFamily(c),CellUtil.cloneQualifier(c));
                    deleteArrayList.add(d);
                }
            }
        }

        try {
            t.delete(deleteArrayList);
            Thread.sleep(100);
        } catch (Exception e) {
           throw new DaoException(e.getMessage(),221);
        }
        return retval;
    }

    @Override
    public List<Map> getbyId(String id, String field,int limit,int offset) throws DaoException {

        List<Map> retval = new ArrayList<>();
        Table t;
        try {
            t =connection.getTable(TableName.valueOf(conf.getServiceName()));
        } catch (IOException e) {
            throw new DaoException(e.getMessage(),6);
        }
        Scan s = new Scan(Bytes.toBytes(id));
        s.addFamily(Bytes.toBytes("fields"));
        try {
            ResultScanner rs = t.getScanner(s);
            Result[] r =  rs.next(limit+offset);
            int count = r.length;
            for(int i=offset;i<count;i++){
                for (Cell cell:r[i].listCells()) {
                    Map<String,String> cellMap = new HashMap<>();
                    String cq = Bytes.toString(CellUtil.cloneQualifier(cell));
                    Boolean add = false;
                    if(field != null && !field.isEmpty()){
                        if(cq.startsWith(field)){
                            add =true;
                        }
                    }else{
                        add = true;
                    }
                    //adding
                    if(add == true){
                        cellMap.put("key",Bytes.toString(CellUtil.cloneRow(cell)));
                        cellMap.put("value",Bytes.toString(CellUtil.cloneValue(cell)));
                        cellMap.put("qualifier",cq);
                        cellMap.put("timestamp",Long.toString(cell.getTimestamp()));
                        retval.add(cellMap);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return retval;
    }

    @Override
    public Map<String,List> getAll(String field,int limit ,int offset) throws DaoException {
        List<Map> retval = new ArrayList<>();
        Map<String,List> retvalMap = new HashMap<>();
        Table t;
        try {
            t =connection.getTable(TableName.valueOf(conf.getServiceName()));
        } catch (IOException e) {
            throw new DaoException(e.getMessage(),6);
        }
        Scan s = new Scan();
        s.addFamily(Bytes.toBytes("fields"));
        try {
            ResultScanner rs = t.getScanner(s);
            Result[] r =  rs.next(limit+offset);
            int count = r.length;
            for(int i=offset;i<count;i++){
                for (Cell cell:r[i].listCells()) {
                    Map<String,String> cellMap = new HashMap<>();
                    String cq = Bytes.toString(CellUtil.cloneQualifier(cell));
                    Boolean add = false;
                    if(field != null && !field.isEmpty()){
                        if(cq.startsWith(field)){
                            add =true;
                        }
                    }else{
                        add = true;
                    }
                    //adding
                    if(add == true){
                        cellMap.put("key",Bytes.toString(CellUtil.cloneRow(cell)));
                        cellMap.put("value",Bytes.toString(CellUtil.cloneValue(cell)));
                        cellMap.put("qualifier",cq);
                        cellMap.put("timestamp",Long.toString(cell.getTimestamp()));
                        retval.add(cellMap);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Map<String,Object> cell:retval) {
            if(retvalMap.containsKey(cell.get("key").toString())){
                List<Map> tmp = retvalMap.get(cell.get("key").toString());
                tmp.add(cell);
                retvalMap.put(cell.get("key").toString(),tmp);
            }else{
                List<Map> tmp = new ArrayList<>();
                tmp.add(cell);
                retvalMap.put(cell.get("key").toString(),tmp);
            }
        }

        return retvalMap;
    }

    @Override
    public List<Map> search(Map<String, Object> keyValue) {
        return null;
    }

    public List<Map> search(String key,String value ,int limit,int offset) throws DaoException {
        List<Map> retval = new ArrayList<>();
        Table t;
        try {
            t =connection.getTable(TableName.valueOf(conf.getServiceName()));
        } catch (IOException e) {
            throw new DaoException(e.getMessage(),6);
        }
        Scan s = new Scan(Bytes.toBytes(value),Bytes.toBytes(value));
        s.addFamily(Bytes.toBytes("lookup"));
        try {
            ResultScanner rs = t.getScanner(s);
            Result[] r =  rs.next(limit+offset);
            int count = r.length;
            for(int i=offset;i<count;i++){
                for (Cell cell:r[i].listCells()) {
                    Map<String,String> cellMap = new HashMap<>();
                    String cq = Bytes.toString(CellUtil.cloneQualifier(cell));
                    Boolean add = false;
                    if(key != null || !key.isEmpty()){
                        if(cq.startsWith(key)){
                            add =true;
                        }
                    }else{
                        add = true;
                    }
                    //adding
                    if(add == true){
                        cellMap.put("key",Bytes.toString(CellUtil.cloneValue(cell)));
                        cellMap.put("value",Bytes.toString(CellUtil.cloneRow(cell)));
                        cellMap.put("qualifier",cq.substring(0,cq.lastIndexOf("#")));
                        cellMap.put("timestamp",Long.toString(cell.getTimestamp()));
                        retval.add(cellMap);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return retval;
    }


    public List<Map> search(String key,String startValue,String stopValue ,int limit,int offset) throws DaoException {
        List<Map> retval = new ArrayList<>();
        Table t;
        try {
            t =connection.getTable(TableName.valueOf(conf.getServiceName()));
        } catch (IOException e) {
            throw new DaoException(e.getMessage(),6);
        }
        Scan s = new Scan(Bytes.toBytes(startValue),Bytes.toBytes(stopValue));
        s.addFamily(Bytes.toBytes("lookup"));
        try {
            ResultScanner rs = t.getScanner(s);
            Result[] r =  rs.next(limit+offset);
            int count = r.length;
            for(int i=offset;i<count;i++){
                for (Cell cell:r[i].listCells()) {
                    Map<String,String> cellMap = new HashMap<>();
                    String cq = Bytes.toString(CellUtil.cloneQualifier(cell));
                    Boolean add = false;
                    if(key != null || !key.isEmpty()){
                        if(cq.startsWith(key)){
                            add =true;
                        }
                    }else{
                        add = true;
                    }
                    //adding
                    if(add == true){
                        cellMap.put("key",Bytes.toString(CellUtil.cloneValue(cell)));
                        cellMap.put("value",Bytes.toString(CellUtil.cloneRow(cell)));
                        cellMap.put("qualifier",cq.substring(0,cq.lastIndexOf("#")));
                        cellMap.put("timestamp",Long.toString(cell.getTimestamp()));
                        retval.add(cellMap);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return retval;
    }


    private  Boolean add(Map<String, Object> values,String id,String parentKey) throws DaoException {
        if(id == null || id.isEmpty()){
            id = UUID.randomUUID().toString();
        }

        //inserting logic

        for (Map.Entry<String,Object> entry:values.entrySet()) {


            if((entry.getValue() instanceof Map) || (entry.getValue() instanceof List)){
                if(entry.getValue() instanceof Map){
                    if(parentKey !=null){
                        add((Map<String,Object>)entry.getValue(),id,parentKey+"#"+entry.getKey().replace("#",""));
                    }else{
                        add((Map<String,Object>)entry.getValue(),id,entry.getKey().replace("#",""));
                    }

                }else{
                    String dummy;
                    String originalParentKey = parentKey;

                    for (Object value:(List<Object>)entry.getValue()) {
                        if(parentKey == null || parentKey.isEmpty()){
                            dummy = entry.getKey().replace("#","")+"#list";
                        }else{
                            dummy = parentKey +"#"+ entry.getKey().replace("#","")+"#list";

                        }

                        if(value instanceof Map){

                            parentKey = null;
                            Map<String,Object> tmp = new HashMap<>();
                            tmp.put("raw",new Gson().toJson(entry.getValue()));
                            tmp.put("object-".replace("#","") +System.currentTimeMillis(),value);
                            add(tmp,id,dummy);
                        }
                        else{
                            Map<String,Object> tmp = new HashMap<>();
                            tmp.put(value.toString(),value);
                            add(tmp,id,dummy);
                            parentKey = null;

                        }
                        parentKey = originalParentKey;
                    }
                }

            }else{
                //insert
                if(parentKey == null || parentKey.isEmpty()){
                    parentKey = entry.getKey().replace("#","");
                }else{
                    parentKey = parentKey +"#"+ entry.getKey().replace("#","");
                }
                //add to hbc
                HbaseCellEntity c = new HbaseCellEntity();
                c.setColoumnQualifier(parentKey);
                c.setRowkey(id);
                c.setValue(entry.getValue().toString());
                hbc.addCell(c);

                if(parentKey.contains("#")){
                    parentKey = parentKey.substring(0,parentKey.lastIndexOf("#"));
                }else{
                    parentKey = null;
                }
            }
        }

        return true;
    }






    private class HbaseCellEntity {
        private String rowkey ;
        private String coloumnFamily = "fields";
        private String coloumnQualifier;
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        private String timeStamp;

        public String getRowkey() {
            return rowkey;
        }

        public void setRowkey(String rowkey) {
            this.rowkey = rowkey;
        }

        public String getColoumnFamily() {
            return coloumnFamily;
        }

        public void setColoumnFamily(String coloumnFamily) {
            this.coloumnFamily = coloumnFamily;
        }

        public String getColoumnQualifier() {
            return coloumnQualifier;
        }

        public void setColoumnQualifier(String coloumnQualifier) {
            this.coloumnQualifier = coloumnQualifier;
        }

        public String getTimeStamp() {
            return timeStamp;
        }

        public void setTimeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
        }

    }

    private class HbaseEntityCollection{
        private  List<HbaseCellEntity> cells = new ArrayList<>();

        public List<HbaseCellEntity> getCells() {
            return cells;
        }

        public  void setCells(List<HbaseCellEntity> cells) {
            this.cells = cells;
        }

        public void addCell(HbaseCellEntity cell){
            cells.add(cell);
        }

        public void removeCell(HbaseCellEntity cell){
            cells.remove(cell);
        }

        public void flush(Boolean update) throws DaoException {
            if(update == null){
                update = false;
            }

            Table t = null;
            try {
                t =connection.getTable(TableName.valueOf(conf.getServiceName()));
            } catch (IOException e) {
                throw new DaoException("cannot find table",6);
            }
            List<Put> fields = new ArrayList<>();
            List<Put> lookups = new ArrayList<>();
            HbaseDataIntigirtyCheck validator = new HbaseDataIntigirtyCheck(conf);
            for (HbaseCellEntity cell:cells) {
                //integrity check

                try {
                    validator.validate(cell.getColoumnQualifier(),cell.getValue(),cell.getRowkey(),update);
                } catch (ValidationException e) {
                    String msg[] = e.getMessage().split("##");
                    throw new DaoException(msg[0],Integer.parseInt(msg[1]));
                }
                Put field = new Put(Bytes.toBytes(cell.getRowkey()));
                Put lookup = null;
                if(!cell.getValue().isEmpty()){
                    Get g = new Get(Bytes.toBytes(cell.getRowkey()));
                    g.addFamily(Bytes.toBytes("fields"));
                    Result r = null;
                    try {
                        r = t.get(g);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if(!r.isEmpty()){
                        for (Cell hc:r.listCells()) {
                            System.out.println(Bytes.toString(CellUtil.cloneQualifier(hc)));
                            System.out.println(Bytes.toString(CellUtil.cloneQualifier(hc)).contains("list"));
                            if(Bytes.toString(CellUtil.cloneQualifier(hc)).contains("#object") || Bytes.toString(CellUtil.cloneQualifier(hc)).contains("#list")){
                                List<Map> deleteListAndObject = search(Bytes.toString(CellUtil.cloneQualifier(hc)),Bytes.toString(CellUtil.cloneValue(hc)),1,0);
                                for (Map<String,Object> del:deleteListAndObject) {
                                    Delete delete = new Delete(Bytes.toBytes(del.get("value").toString().replaceAll("#","").toLowerCase()));
                                    delete.addColumn(Bytes.toBytes("lookup"),Bytes.toBytes(del.get("qualifier").toString()+"#"+del.get("key").toString()),Long.parseLong(del.get("timestamp").toString()));
                                    try {
                                        t.delete(delete);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            if(Bytes.toString(CellUtil.cloneRow(hc)).equals(cell.getRowkey()) &&
                                    Bytes.toString(CellUtil.cloneQualifier(hc)).equals(cell.getColoumnQualifier())){
                                //cell.getColoumnQualifier() +"#" +cell.getRowkey()
                                if(!Bytes.toString(CellUtil.cloneValue(hc)).equals(cell.getValue())){
                                    if(!Bytes.toString(CellUtil.cloneValue(hc)).isEmpty()){
                                        Delete delete = new Delete(Bytes.toBytes(Bytes.toString(CellUtil.cloneValue(hc)).replaceAll("#","").toLowerCase()));
                                        delete.addColumn(Bytes.toBytes("lookup"),Bytes.toBytes(cell.getColoumnQualifier() +"#" +cell.getRowkey()));
                                        try {
                                            t.delete(delete);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                            }
                            System.out.println(Bytes.toString(CellUtil.cloneQualifier(hc)).contains("list"));
                            System.out.println(Bytes.toString(CellUtil.cloneQualifier(hc)));

                            if(Bytes.toString(CellUtil.cloneQualifier(hc)).contains("list") || Bytes.toString(CellUtil.cloneQualifier(hc)).contains("object")){
                                Delete delete = new Delete(CellUtil.cloneRow(hc));
                                delete.addColumn(Bytes.toBytes("fields"),CellUtil.cloneQualifier(hc),hc.getTimestamp());
                                try {
                                    t.delete(delete);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                    }
                    if(!cell.coloumnQualifier.endsWith("#list#raw")){
                        lookup = new Put(Bytes.toBytes(cell.getValue().replaceAll("#","").toLowerCase()));
                        lookup.addColumn(Bytes.toBytes("lookup"),Bytes.toBytes(cell.getColoumnQualifier() + "#"+cell.getRowkey()),Bytes.toBytes(cell.getRowkey()));
                        lookups.add(lookup);
                    }

                }
                field.addColumn(Bytes.toBytes(cell.getColoumnFamily()),Bytes.toBytes(cell.getColoumnQualifier()),Bytes.toBytes(cell.getValue()));
                fields.add(field);

            }
            try {
                t.put(fields);
            } catch (IOException e) {
                throw new DaoException(e.getMessage(),7);
            }
            try {
                t.put(lookups);
            } catch (IOException e) {
                throw new DaoException(e.getMessage() +"look up",7);
            }
        }

    }




}
