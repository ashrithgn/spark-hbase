package Framework.ORM.Adapter;

import Framework.ORM.Adapter.interfaces.AdapterImplementation;

import java.util.List;
import java.util.Map;

/**
 * Created by ashrith on 6/12/16.
 */
public class Mysql implements AdapterImplementation {

    @Override
    public Boolean add(Map<String, Object> values,String id) {
        return null;
    }

    @Override
    public Boolean update(Map<String, Object> values,String id) {
        return null;
    }

    @Override
    public Boolean Delete(String id, String field) {
        return null;
    }

    @Override
    public List<Map> getbyId(String id, String field,int limit,int offset) {
        return null;
    }

    @Override
    public Map<String,List> getAll(String field,int limit ,int offset) {
        return null;
    }

    @Override
    public List<Map> search(Map<String, Object> keyValue) {
        return null;
    }

    @Override
    public List<Map> search(String key, String value,int limit,int offset) {
        return null;
    }
}
