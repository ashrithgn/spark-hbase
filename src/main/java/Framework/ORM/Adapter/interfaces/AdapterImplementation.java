package Framework.ORM.Adapter.interfaces;

import Framework.Exception.DaoException;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashrith on 6/12/16.
 */
public interface AdapterImplementation {
    Boolean add(Map<String, Object> values, String id) throws DaoException;
    Boolean update(Map<String, Object> values, String id) throws DaoException;
    Boolean Delete(String id, String field) throws DaoException;
    List<Map>getbyId(String id, String field, int limit, int offset) throws DaoException;
    Map<String,List> getAll(String field, int limit, int offset) throws DaoException;
    List<Map> search(Map<String, Object> keyValue);
    List<Map> search(String key, String value, int limit, int offset) throws DaoException;

}
