package Framework.ORM.Facotries;

import Framework.Conf.FrameworkConfiguration;
import Framework.Exception.FactoriesException;
import Framework.ORM.Adapter.Mysql;
import Framework.ORM.Facotries.Interfaces.ConnectionImplementation;

/**
 * Created by ashrith on 31/12/16.
 */
public class MysqlFactory extends Mysql implements ConnectionImplementation {


    public MysqlFactory(FrameworkConfiguration conf) throws FactoriesException {
        initConntection();
    }

    @Override
    public Object initConntection() throws FactoriesException {
        throw new FactoriesException("not yet implemented",2);
    }

    @Override
    public boolean closeConnection() {

        return true;
    }

    @Override
    public Object getConnection() {
        return null;
    }
}
