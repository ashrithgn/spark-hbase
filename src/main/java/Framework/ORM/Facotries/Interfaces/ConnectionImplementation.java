package Framework.ORM.Facotries.Interfaces;

import Framework.Exception.FactoriesException;

/**
 * Created by ashrith on 31/12/16.
 */
public interface ConnectionImplementation {
   Object initConntection() throws FactoriesException;
   Object getConnection();
   boolean closeConnection();
}
