package Framework.ORM.Facotries;

import Framework.Conf.FrameworkConfiguration;
import Framework.ORM.Adapter.Hbase;
import Framework.ORM.Facotries.Interfaces.ConnectionImplementation;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

/**
 * Created by ashrith on 31/12/16.
 */
public class HbaseFactory extends Hbase implements ConnectionImplementation {
    private static org.apache.hadoop.conf.Configuration hConfig = HBaseConfiguration.create();

    public HbaseFactory(FrameworkConfiguration conf) {
        hConfig.addResource(conf.getDbConnection().get("sitepath"));
        hConfig.addResource(conf.getDbConnection().get("sitepolicy"));
        Hbase.conf = conf;
        initConntection();
    }

    @Override
    public Object initConntection() {
         Hbase.connection = null;
        try{
            Hbase.connection = ConnectionFactory.createConnection(hConfig);

        }catch(Exception e){}
        return Hbase.connection;
    }

    @Override
    public boolean closeConnection() {
        try {
            Hbase.connection.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Object getConnection() {
        return Hbase.connection;
    }
}
