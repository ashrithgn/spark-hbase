package Framework.Conf;

import Framework.Exception.ConfigurationException;
import Framework.Exception.JsonreaderException;
import Utlity.Jsonreader;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Created by root on 5/12/16.
 */
public class FrameworkConfiguration {

    private static List<Map> fields = null;
    private static Map<String,Object>  systemConfiguration = null;
    private static String port = null;
    private static String host = null;
    private static String database = null;
    private static String serviceName = null;
    private static Map<String,String> dbConnection = null;
    private static List<String> fieldIndex = null;
    private static Map<String,String> fieldHash = null;
    private static String logpath = "/var/logs/";

    private FrameworkConfiguration() {

    }




    public FrameworkConfiguration(String path) throws JsonreaderException, ConfigurationException {
        Map<String,Object> config = Jsonreader.fromFile(path);

        if(!(config.get("fields") instanceof List)){
            throw new ConfigurationException("malformed fieldlist",5);
        }

        if(!(config.get("systemConfiguration") instanceof Map)){
            throw new ConfigurationException("malformed systemconfiguration",6);
        }
        fields = (List) config.get("fields");
        systemConfiguration = (Map<String, Object>) config.get("systemConfiguration");
        port = config.get("port").toString();
        database = config.get("database").toString();
        host = config.get("host").toString();
        serviceName = config.get("serviceName").toString();
        dbConnection = (Map<String,String>)config.get("connectionsetup");
        fieldIndex = new ArrayList<>();
        fieldHash = new HashMap<>();
        fieldRoot(fields,null);
        logpath = config.get("logpath").toString();
    }

    public static List<String> getFieldIndex() {
        return fieldIndex;
    }

    public  List<Map> getFields() {
        return fields;
    }

    public  Map<String,Object> getSystemConfiguration() {
        return systemConfiguration;
    }

    public String getPort() {
        return port;
    }

    public  String getHost() {
        return host;
    }

    public  String getDatabase() {
        return database;
    }

    public  String getServiceName() {
        return serviceName;
    }

    public  List<Map> modifyFields(List<Map> fields) {
        FrameworkConfiguration.fields = fields;
        return fields;
    }

    public  Map<String,String> getDbConnection() {
        return dbConnection;
    }



    private void fieldRoot(List<Map> fields,String key) {
        for (Map<String,Object> field:fields) {
            String name;
            if(key == null){
                name=field.get("name").toString().replaceAll("#","");
            }else{
                name= key + "#"+ field.get("name").toString().replaceAll("#","");
            }

            fieldIndex.add(name);
            fieldHash.put(name,field.get("name").toString());

            if(field.get("type").equals("object")){
                if(!(field.get("enum") instanceof String)){
                    fieldRoot((List<Map>) field.get("enum"),name);
                }


            }
        }
    }

    public static Map<String, String> getFieldHash() {
        return fieldHash;
    }

    public static String getLogPath() {
        return logpath;
    }
}
