package Framework.Conf;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;

import java.io.IOException;

/**
 * Created by ashrith on 6/12/16.
 */
public class FrameworkHabseConfiguration {

    public FrameworkHabseConfiguration(FrameworkConfiguration conf) throws IOException {
        org.apache.hadoop.conf.Configuration hConfig = HBaseConfiguration.create();
        hConfig.addResource(conf.getDbConnection().get("sitepath"));
        hConfig.addResource(conf.getDbConnection().get("sitepolicy"));
        Connection connection = ConnectionFactory.createConnection(hConfig);
        Admin admin = connection.getAdmin();
        if(!admin.isTableAvailable(TableName.valueOf(conf.getServiceName()))){
            HTableDescriptor dis = new HTableDescriptor(TableName.valueOf(conf.getServiceName()));
            HColumnDescriptor family = new HColumnDescriptor("fields");
            HColumnDescriptor family1 = new HColumnDescriptor("lookup");
            dis.addFamily(family);
            dis.addFamily(family1);
            admin.createTable(dis);
        }
        connection.close();

    }

}
