package Framework;


import Framework.Exception.ConfigurationException;
import Framework.Exception.DaoException;
import Framework.Exception.JsonreaderException;
import Framework.Conf.FrameworkConfiguration;
import Framework.Logger.Log;
import Framework.ORM.Adapter.interfaces.AdapterImplementation;
import Framework.ORM.Dao;
import Framework.ORM.OutputProcessor;
import com.google.gson.Gson;
import spark.Request;
import spark.Response;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by root on 5/12/16.
 */
public class Init {
    public FrameworkConfiguration config;
    public static AdapterImplementation dao;
    public static OutputProcessor processor;
    private static Log log;

    public Init(String path) throws JsonreaderException, ConfigurationException, DaoException {
        config = new FrameworkConfiguration(path);
        runSetup();

    }

    private void runSetup() throws ConfigurationException, DaoException {
        dao =Dao.getInstance(config);
        processor = new OutputProcessor(config);
        log = new Log(config,null,null);

    }

    public FrameworkConfiguration exportConfig(){
        return this.config;
    }

    public static AdapterImplementation getDao() {
        return dao;
    }

    public OutputProcessor getOutputProcessor(){
        return processor;
    }

    public static Log getLog() {
        return log;
    }

    public static void  sparkLogWriter(Request request, Response response){
        Map<String, Object> LogResponse = new HashMap<String, Object>();
        LogResponse.put("Req-body", new Gson().fromJson(request.body(),HashMap.class));
        LogResponse.put("Req-url", request.url());
        LogResponse.put("Req-headers", request.headers());
        for (String headename : request.headers()) {
            LogResponse.put("Req-header-" + headename, request.headers(headename));
        }
        LogResponse.put("Req-params", request.params());
        LogResponse.put("Req-queryString", request.queryString());
        LogResponse.put("Res-body", new Gson().fromJson(response.body(),HashMap.class));
        LogResponse.put("Res-date", new Date().toString());
        log.log(LogResponse,"debug");
    }
}
