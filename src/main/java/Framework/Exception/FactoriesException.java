package Framework.Exception;

/**
 * Created by ashrith on 31/12/16.
 */
public class FactoriesException extends Exception{
    public FactoriesException(String message , int code) {
        super(message + "##" + code);
    }
}
