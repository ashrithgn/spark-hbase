package Framework.Exception.Http;

/**
 * Created by costrategix on 9/19/16.
 */
public class AuthException extends Exception{
    /**
     * throws auth exception
     *
     * @param S error message
     */
    public AuthException(String S,String code) {
        super(S + "##" +code);
    }
}
