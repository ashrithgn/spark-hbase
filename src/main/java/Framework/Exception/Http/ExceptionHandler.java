package Framework.Exception.Http;

import Framework.Init;
import com.google.gson.Gson;
import spark.Request;
import spark.Response;
import java.util.Date;
import java.util.HashMap;

/**
 * created to maps custom exceptions to spark exceptions class
 * Created by costrategix on 9/15/16.
 */
public class ExceptionHandler {
	
	 /**
     * implementation of empty post request
     *
     * @param exception Exception
     * @param request   spark
     * @param response  spark
     */
    public static void genericException(Exception exception, Request request , Response response){
        exception.printStackTrace();

    }
    
    /**
     * implementation of empty post request
     *
     * @param exception Exception
     * @param request   spark
     * @param response  spark
     */
    public static void emptyRequest(Exception exception, Request request , Response response){
        String[] exceptionbody = exception.getMessage().split("##");
        response.header("Content-Type","application/json");
        response.status(400);
        HashMap<String, Object> body = new HashMap<>();
        body.put("status","0");
        body.put("error", exceptionbody[0]);
        body.put("errorcode", exceptionbody[1]);
        String responseBody = new Gson().toJson(body);
        response.body(responseBody);
        logException(request,response);

    }

    /**
     * handles  required paramter exceptions
     * @param exception Exception
     * @param request spark
     * @param response spark
     */
    public static void requiresParam(Exception exception, Request request , Response response){
        String[] exceptionbody = exception.getMessage().split("##");
        response.header("Content-Type","application/json");
        response.status(422);
        HashMap<String,Object> body = new HashMap<String,Object>();
        body.put("status","0");
        body.put("error", exceptionbody[0]);
        body.put("errorcode", exceptionbody[1]);
        String responseBody = new Gson().toJson(body);
        response.body(responseBody);
        logException(request,response);
    }

    /**
     * handles validation Exception
     * @param exception exception
     * @param request spark
     * @param response spark
     */
    public static void validationError(Exception exception, Request request , Response response){
        String[] exceptionbody = exception.getMessage().split("##");
        response.header("Content-Type","application/json");
        response.status(422);
        HashMap<String,Object> body = new HashMap<String,Object>();
        body.put("status","0");
        body.put("error", exceptionbody[0]);
        body.put("errorcode", exceptionbody[1]);
        String responseBody = new Gson().toJson(body);
        response.body(responseBody);
        logException(request,response);
    }

    /**
     * auth validation Exception
     * @param exception exception
     * @param request spark
     * @param response spark
     */
    public static void authError(Exception exception, Request request, Response response) {
        response.header("Content-Type", "application/json");
        response.status(401);
        String[] exceptionbody = exception.getMessage().split("##");
        HashMap<String, Object> body = new HashMap<String, Object>();
        body.put("status", "0");
        body.put("error", exceptionbody[0]);
        body.put("errorcode", exceptionbody[1]);
        String responseBody = new Gson().toJson(body);
        response.body(responseBody);
        logException(request,response);
    }


    public static void daoException(Exception exception, Request request, Response response) {
        response.header("Content-Type", "application/json");
        response.status(422);
        String[] exceptionbody = exception.getMessage().split("##");
        HashMap<String, Object> body = new HashMap<String, Object>();
        body.put("status", "0");
        body.put("error", exceptionbody[0]);
        body.put("errorcode", exceptionbody[1]);
        String responseBody = new Gson().toJson(body);
        response.body(responseBody);
        logException(request,response);
    }

    public static void internalServerError(Exception exception, Request request, Response response) {
        response.header("Content-Type", "application/json");
        response.status(500);

        HashMap<String, Object> body = new HashMap<String, Object>();
        body.put("status", "0");
        body.put("error", exception.getMessage() + " cause " + exception.getCause());
        body.put("errorcode","500");
        body.put("stacktrace", exception.getStackTrace());
        String responseBody = new Gson().toJson(body);
        response.body(responseBody);
        logException(request,response);
    }

    public static void logException(Request request, Response response){

        HashMap<String , Object> LogResponse = new HashMap<String, Object>();
        LogResponse.put("Req-body",request.body());
        LogResponse.put("Req-url",request.url());
        LogResponse.put("Req-headers",request.headers());
        for (String headename:request.headers()){
            LogResponse.put("Req-header-" +headename,request.headers(headename));
        }
        LogResponse.put("Req-params",request.params());
        LogResponse.put("Req-queryString",request.queryString());
        LogResponse.put("Res-body",response.body());
        LogResponse.put("Res-date",new Date().toString());
        Init.getLog().log(LogResponse,"exception");

    }
}
