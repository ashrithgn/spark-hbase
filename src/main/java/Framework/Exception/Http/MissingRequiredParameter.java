package Framework.Exception.Http;

/**
 * Created by costrategix on 9/15/16.
 */
public class MissingRequiredParameter extends Exception{
    /**
     * throws required params error
     *
     * @param S error message
     */
    public MissingRequiredParameter(String S,String code) {
        super(S + "##" +code);
    }
}
