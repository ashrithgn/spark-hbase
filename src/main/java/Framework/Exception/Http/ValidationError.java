package Framework.Exception.Http;

/**
 * Created by costrategix on 9/15/16.
 */
public class ValidationError extends Exception{
    /**
     * throws general validation error
     *
     * @param S validation message
     */
    public ValidationError(String S,String code) {
        super(S + "##" +code);
    }
}
