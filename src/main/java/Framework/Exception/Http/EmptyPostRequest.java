package Framework.Exception.Http;

/**
 * Created by costrategix on 9/15/16.
 */
public class EmptyPostRequest extends Exception {
    /**
     * throws Empty post request exception
     *
     * @param S error message
     */
    public EmptyPostRequest(String S,String code) {
        super(S + "##" +code);
    }
}
