package Framework.Exception;

/**
 * Created by root on 5/12/16.
 */
public class ConfigurationException extends Exception {
    public ConfigurationException(String message,int code) {
        super(message + "##" + code);
    }
}