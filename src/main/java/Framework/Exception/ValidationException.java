package Framework.Exception;

/**
 * Created by ashrith on 10/1/17.
 */
public class ValidationException extends Exception {
    public ValidationException(String message,int code) {
        super(message + "##" + code);
    }
}
