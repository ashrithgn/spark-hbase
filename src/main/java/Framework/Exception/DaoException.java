package Framework.Exception;

/**
 * Created by ashrith on 31/12/16.
 */
public class DaoException extends Exception{
    public DaoException(String message , int code ) {
        super(message + "##" + code);
    }
}
