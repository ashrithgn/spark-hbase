package Framework.Logger;

import org.apache.hadoop.hbase.util.Bytes;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

/**
 * Created by ashrith on 20/1/17.
 */
public class UdpServer {
    static int  port;
    static String logPath;

    public UdpServer(int port, String logPath) {
        DatagramSocket serverSocket = null;
        UdpServer.port =port;
        UdpServer.logPath = logPath;
        try {
            serverSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        DatagramSocket finalServerSocket = serverSocket;
        Thread td = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    byte[] receiver = new byte[66566];
                    try {
                        DatagramPacket dt = new DatagramPacket(receiver,receiver.length);
                        finalServerSocket.receive(dt);

                        byte data[] = new byte[dt.getData().length];
                        data = dt.getData();
                        String recievedData = new String(data);
                        recievedData = recievedData.substring(0,recievedData.lastIndexOf(";"));
                        List<String> lines = new ArrayList<>();
                        lines.add(recievedData);
                        java.util.Date date = new java.util.Date();
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        String coustomFomat = dateFormat.format(date).replaceAll("[-]", "");
                        String mountPoint = logPath +"/"+coustomFomat;
                        String fileName = "access.log";
                        File logfile = new File(mountPoint + "/");
                        logfile.mkdirs();
                        logfile.createNewFile();
                        Files.write(Paths.get(mountPoint + "/" + fileName), lines, UTF_8, APPEND, CREATE);
                        lines.clear();
                    } catch (SocketException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        td.start();
    }

}
