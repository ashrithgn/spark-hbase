package Framework.Logger;

import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.net.*;

/**
 * Created by ashrith on 20/1/17.
 */
public class UdpClient {
    static DatagramSocket socket;
    static InetAddress host;
    static int port;

    public UdpClient(int port ,String address) {
        try {
            socket =new DatagramSocket();
            host = InetAddress.getByName(address);
            UdpClient.port = port;
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public void send(String data){
        if(socket != null){
            byte[] messageInByte = data.getBytes();
            DatagramPacket dp = new DatagramPacket(messageInByte , messageInByte.length , host , port);
            try {
                socket.send(dp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
