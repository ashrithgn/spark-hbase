package Framework.Logger;

import Framework.Conf.FrameworkConfiguration;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by ashrith on 20/1/17.
 */
public class Log {
    private static String serviceName = "default";
    private  static String separator = ";" ;
    private  static String carriageReturn = "\n" ;
    private  static  UdpServer u = null;
    private  static  FrameworkConfiguration conf = null;
    private  static UdpClient uc = null;

    public Log(FrameworkConfiguration conf,String separator,String carriageReturn) {
        Log.conf = conf;
        if(separator != null && !separator.isEmpty()){
            Log.separator = separator;
        }

        if(carriageReturn != null){
            Log.carriageReturn = carriageReturn;
        }

        serviceName = conf.getServiceName();
        if(u == null){
            u = new UdpServer(Integer.parseInt(conf.getPort() + "0"), FrameworkConfiguration.getLogPath());
        }

        if(uc == null){
            uc = new UdpClient(Integer.parseInt(conf.getPort() + "0"),conf.getHost());
        }

    }

    public  void log(Map<String,Object> actionAttributes, String type){
        new Thread(new Runnable() {
            @Override
            public void run() {
                java.util.Date sysDate = new java.util.Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateFormat.format(sysDate);
                String logBuilder = date.replaceAll(separator,"#")+separator+serviceName.replaceAll(separator,"#")+separator+"applicationlog"+ separator + type +separator + new Gson().toJson(actionAttributes) + separator+carriageReturn;
                System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
                System.out.println(logBuilder);
                if(uc != null){
                    uc.send(logBuilder);
                }

            }
        }).run();


    }

    public  void log(String actionAttributes, String type){
        new Thread(new Runnable() {
            @Override
            public void run() {
                java.util.Date sysDate = new java.util.Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateFormat.format(sysDate);
                String logBuilder = date.replaceAll(separator,"#")+separator+serviceName.replaceAll(separator,"#")+separator+"applicationlog"+ separator + type +separator + new Gson().toJson(actionAttributes) + separator+carriageReturn;
                System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
                System.out.println(logBuilder);
                if(uc != null){
                    uc.send(logBuilder);
                }

            }
        }).run();


    }

    public  void eventLog(String userId,String eventId, String eventName,String action, Map<String,Object> actionAttributes){
        new Thread(new Runnable() {
            @Override
            public void run() {
                java.util.Date sysDate = new java.util.Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateFormat.format(sysDate);
                String logBuilder = date.replaceAll(separator,"#")+separator+userId.replaceAll(separator,"#")+separator+serviceName.replaceAll(separator,"#")+separator+eventId.replaceAll(separator,"#") + separator + eventName.replaceAll(separator,"#") +separator+action.replaceAll(separator,"#")+separator + new Gson().toJson(actionAttributes) + separator+carriageReturn;
                System.out.println(logBuilder);
                if(uc != null){
                    uc.send(logBuilder);
                }
            }
        }).start();
    }

    public  void eventLog(String userId,String eventId, String eventName,String action, String actionAttributes){
        new Thread(new Runnable() {
            @Override
            public void run() {
                java.util.Date sysDate = new java.util.Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateFormat.format(sysDate);
                String logBuilder = date.replaceAll(separator,"#")+separator+userId.replaceAll(separator,"#")+separator+serviceName.replaceAll(separator,"#")+separator+eventId.replaceAll(separator,"#") + separator + eventName.replaceAll(separator,"#") +separator+action.replaceAll(separator,"#")+separator + new Gson().toJson(actionAttributes) + separator+carriageReturn;
                System.out.println(logBuilder);
                if(uc != null){
                    uc.send(logBuilder);
                }
            }
        }).start();
    }
}
